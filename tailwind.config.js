module.exports = {
  content: [
    "./src/**/*.{js,jsx,ts,tsx}",
    "./node_modules/tw-elements/dist/js/**/*.js",
    "./src/**/*.{html,js}",
  ],
  theme: {
    extend: {
      width: {
        620: "620px",
        760: "750px",
      },
      maxWidth: {
        9: "9rem",
      },
      height: {
        620: "100%",
        600: "42rem",
        429: "28rem",
        500: "500px",
        610: "80rem",
        605: "600px",
        700: "700px",
      },
      padding: {
        620: "35rem",
        600: "33rem",
        610: "43rem",
      },
      margin: {
        51: "40rem",
      },
    },

    screenLeft: {
      30: "30rem",
    },
    maxWidth: {
      14: "14rem",
      16: "16rem",
      18: "19rem",
    },
    maxHeight: {
      14: "14rem",
      16: "16rem",
      31: "40rem",
    },
    screens: {
      phone: "375px", // duoi 375 none
      ipad: "768px", // duoi
      desktop: "1519px",
      pc: "1700px",
      sn: "375px",
      sm: "640px",
      md: "768px",
      lg: "1024px",
      xl: "1280px",
      xll: "1519px",
      xxl: "1700px",
    },
  },

  plugins: ["tw-elements/dist/plugin"],
};
