import React from "react";
import { useSelector } from "react-redux";
import { logcalStorageService } from "../../services/localStorageService";
// import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";

export default function UserNav() {
  let userInfor = useSelector((state) => state.userReducer.userInfor);
  let handleLogout = () => {
    logcalStorageService.removeUserInfor();
    // logout xong quay ve trang dang nhap
    window.location.href = "/login";
  };
  let handleLogin = () => {
    window.location.href = "/login";
  };
  let handleRegister = () => {
    window.location.href = "/dangKy";
  };
  return (
    <div>
      {userInfor ? (
        <div className="space-x-3 mt-3">
          {" "}
          <span className="text-orange-600 text-base font-normal">
            {userInfor?.hoTen}
          </span>{" "}
          <button
            onClick={handleLogout}
            type="button"
            class=" bg-cyan-700 hover:bg-blue-500 text-white  font-medium rounded-lg text-sm  
           phone:px-4 ipad:px-5 desktop:px-6 pc:px-7 py-2.5 text-center mr-2 mb-2"
          >
            Đăng xuất
          </button>
        </div>
      ) : (
        <div className="space-x-3 mt-2">
          <button
            class="bg-cyan-700 hover:bg-blue-500 text-white font-medium rounded-lg text-sm  
            phone:px-4 ipad:px-5 desktop:px-6 pc:px-7 py-2.5 text-center mr-2 mb-2"
            onClick={handleLogin}
          >
            Đăng nhập
          </button>
          <button
            class="bg-cyan-700 hover:bg-blue-500 text-white  font-medium rounded-lg text-sm  
             phone:px-4 ipad:px-5 desktop:px-6 pc:px-7 py-2.5 text-center mr-2 mb-2"
            onClick={handleRegister}
          >
            Đăng kí
          </button>
        </div>
      )}
    </div>
  );
}
