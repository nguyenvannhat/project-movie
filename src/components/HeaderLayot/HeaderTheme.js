import React, { Fragment } from "react";
import { NavLink } from "react-router-dom";
import MovieTabs from "../../pages/HomePage/MovieTabs/MovieTabs";
import { AiOutlineBars } from "react-icons/ai";
import { FaSubscript } from "react-icons/fa";
import UserNav from "./UserNav";
import "./HeaderTheme.css";

export default function HeaderTheme() {
  return (
    <div className="">
      <nav className="xll:bg-white xll:w-full border-gray-500 ">
        <div className=" flex flex-wrap justify-between items-center mx-auto max-w-screen-xl px-4 md:px-6 py-2.5 ">
          <a href="/" className="flex items-center">
            <img
              src={require("../../assets/nhat.jpg")}
              className="mr-3 h-10 sm:h-11"
              alt=""
            />
          </a>
          <div className="flex items-center mt-2 mb-3 phone:hidden ipad:block desktop:block pc:block">
            <UserNav />
          </div>
        </div>
      </nav>
      <div>
        <div className=" phone:bg-slate-400 ipad:bg-slate-600  desktop:bg-slate-800  pc:bg-yellow-700 ">
          <nav class="navbar navbar-expand-lg navbar-light bg-slate-700  ipad:hidden desktop:hidden ">
            <a class="navbar-brand" href="#">
              <UserNav />
            </a>
            <button
              class="navbar-toggler bg-white"
              type="button"
              data-toggle="collapse"
              data-target="#navbarNav"
              aria-controls="navbarNav"
              aria-expanded="false"
              aria-label="Toggle navigation"
            >
              <span class="navbar-toggler-icon "></span>
            </button>
            <div class="collapse navbar-collapse " id="navbarNav">
              <ul class="navbar-nav ">
                <li class="nav-item active">
                  <a
                    class="nav-link text-white text-base font-light"
                    href="/muave"
                  >
                    MUA VÉ <span class="sr-only">(current)</span>
                  </a>
                </li>
                <li class="nav-item ">
                  <a
                    class="nav-link text-white text-base font-light"
                    href="/PageFilm"
                  >
                    PHIM
                  </a>
                </li>
                <li class="nav-item">
                  <a
                    class="nav-link text-white text-base font-light"
                    href="/khuyenMai"
                  >
                    SỰ KIỆN
                  </a>
                </li>
                <li class="nav-item">
                  <a
                    class="nav-link text-white text-base font-light "
                    href="/giaVe"
                  >
                    RẠP/GIÁ VÉ
                  </a>
                </li>
                <li class="nav-item">
                  <a
                    class="nav-link text-white text-base font-light"
                    href="/support"
                  >
                    HỖ TRỢ
                  </a>
                </li>
              </ul>
            </div>
          </nav>
          <nav className=" px-10 phone:hidden ipad:block desktop:block pc:block  tablet:h-28 desktop:h-16 ">
            {" "}
            <div className="pt-1 max-w-screen-xl md:px-6 h-18">
              <div className="text-center  ">
                <ul className="pt-3 grid  desktop:grid-cols-6 tablet:grid-cols-5  phone:grid-cols-3   gap-5  text-center space-x-8 text-sm font-medium">
                  <li>
                    <a
                      href="/muave"
                      className="text-white hover:underline "
                      aria-current="page"
                    >
                      MUA VÉ
                    </a>
                  </li>
                  <li>
                    <a href="/PageFilm" className="text-white hover:underline ">
                      PHIM
                    </a>
                  </li>
                  <li>
                    <a
                      href="/khuyenMai"
                      className="text-white hover:underline "
                    >
                      SỰ KIỆN
                    </a>
                  </li>
                  <li>
                    <a href="/giaVe" className="text-white hover:underline   ">
                      RẠP/GIÁ VÉ
                    </a>
                  </li>
                  <li>
                    <a href="/support" className="text-white hover:underline ">
                      HỖ TRỢ
                    </a>
                  </li>
                  <li>
                    <a href="#" className="text-white hover:underline ">
                      THÀNH VIÊN
                    </a>
                  </li>
                </ul>
              </div>
            </div>
          </nav>
        </div>
      </div>
    </div>
  );
}
