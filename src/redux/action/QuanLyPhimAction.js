import React from "react";
import { MovieService } from "../../services/MovieService";
import { Quan_LY_PHIM } from "../constants/danhSachPhimConstants";

export default function QuanLyPhimAction(phim) {
  return {
    type: Quan_LY_PHIM,
    payload: phim,
  };
}
export const ThemPhimUploadHinhAction = (formData) => {
  return async (dispatch) => {
    try {
      let result = await MovieService.postThemPhimUpLoadHinh(formData);
      alert("them thanh cong");
      console.log(" them thanh cong", result.data.content);
    } catch (err) {
      console.log(err.response?.data);
    }
  };
};
