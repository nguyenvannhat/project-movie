import { QuanLyDatVeService } from "../../services/QuanLyDatVeService";
import { SET_CHI_TIET_PHONG_VE } from "../constants/datVeConstants";

export const QuanLyDatVeAction = (ve) => {
  return {
    type: SET_CHI_TIET_PHONG_VE,
    payload: ve,
  };
};
// export const datVe = (thongTinDatVe = new ThongTinDatVe()) => {
//   return async dispatch => {
//     try {
//       const result = await QuanLyDatVeService.postMovieTicKet(thongTinDatVe);
//     } catch (error) {
//       console.log(error.response.data);
//     }
//   };
// };
