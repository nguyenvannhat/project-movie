import { LIST_FILM } from "../constants/ListFilmConstants";

export const QuanLyListFilmAction = (film) => {
  return {
    type: LIST_FILM,
    payload: film,
  };
};
