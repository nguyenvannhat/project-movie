import { SET_USER_INFOR } from "../constants/userConstants";

export const setUserInforAtion = (user) => {
  return {
    type: SET_USER_INFOR,
    payload: user,
  };
};
