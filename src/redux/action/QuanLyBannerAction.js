import React from "react";
import { BANNER } from "../constants/bannerConstants";

export const QuanLyBannerAction = (banner) => {
  return {
    type: BANNER,
    payload: banner,
  };
};
