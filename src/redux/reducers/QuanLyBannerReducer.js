import { logcalStorageService } from "../../services/localStorageService";
import { BANNER } from "../constants/bannerConstants";

let initalState = {
  arrayBanner: logcalStorageService.getBanner(),
};

export const QuanLyBannerReducer = (state = initalState, action) => {
  switch (action.type) {
    case BANNER: {
      state.arrayBanner = action.payload;
      return { ...state };
    }
    default:
      return state;
  }
};
