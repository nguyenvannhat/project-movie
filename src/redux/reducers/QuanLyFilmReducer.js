import React from "react";
import { logcalStorageService } from "../../services/localStorageService";
import { LIST_FILM } from "../constants/ListFilmConstants";

let initalState = {
  ArrayListFilm: logcalStorageService.getListFilmInfor(),
};
export const QuanLyFilmReducer = (state = initalState, action) => {
  switch (action.type) {
    case LIST_FILM: {
      state.ArrayListFilm = action.payload;
      return { ...state };
    }
    default:
      return state;
  }
};
