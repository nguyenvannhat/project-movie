import { logcalStorageService } from "../../services/localStorageService";
import { Quan_LY_PHIM } from "../constants/danhSachPhimConstants";

let initalState = {
  ArrayFilm: logcalStorageService.getDanhSachPhim(),
  ArrayFilmDefault: [],
};
export const QuanLyPhimReducer = (state = initalState, action) => {
  switch (action.type) {
    case Quan_LY_PHIM: {
      state.ArrayFilmDefault = action.ArrayFilm;
      return { ...state };
    }
    default:
      return state;
  }
};
