import { combineReducers, createStore } from "redux";
import { userReducer } from "./userReducer";
import { QuanLyDatVeReducer } from "./QuanLyDatVeReducer";
import { QuanLyPhimReducer } from "./QuanLyPhimReducer";
export const rootReducer = combineReducers({
  userReducer,
  QuanLyDatVeReducer,
  QuanLyPhimReducer,
});

export const store = createStore(
  rootReducer,
  window.__REDUX_DEVTOOLS_EXTENSION__ && window.__REDUX_DEVTOOLS_EXTENSION__()
);
