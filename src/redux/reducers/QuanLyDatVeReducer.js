import { logcalStorageService } from "../../services/localStorageService";
import { DAT_VE, SET_CHI_TIET_PHONG_VE } from "../constants/datVeConstants";

let initalState = {
  chiTietPhongVe: logcalStorageService.getDatVeInfor(),
  // khi user click vao o dat ve thi thong tin ve se gui vao cai mang nay
  danhSachGheDangDat: [],
};
export const QuanLyDatVeReducer = (state = initalState, action) => {
  switch (action.type) {
    case SET_CHI_TIET_PHONG_VE: {
      state.chiTietPhongVe = action.payload;
      return { ...state };
    }
    case DAT_VE: {
      // cập nhật danh sách ghế đang đặt nếu user kích vào lần hai tức là hủy ghế đang đặt
      let danhSachGheCapNhat = [...state.danhSachGheDangDat];
      // dùng danh sách ghế cập nhật coi có ghế mình gửi lên hay không nếu có thì đang nằm trong mảng ghế
      let index = danhSachGheCapNhat.findIndex(
        (gheDD) => gheDD.maGhe === action.gheDuocChon.maGhe
      );
      // cụ thể mình đem từng cái ghế trong mảng  danhSachGheDangDat so sánh với action ghe được chọn
      if (index != -1) {
        // nếu tìm thấy ghế được chọn trong mảng thì có nghĩa trước đó đã click vào rồi  => xóa đi
        danhSachGheCapNhat.splice(index, 1);
      } else {
        // ngược lại chưa có thì cho nó vào
        danhSachGheCapNhat.push(action.gheDuocChon);
      }
      console.log(" action", action);
      return { ...state, danhSachGheDangDat: danhSachGheCapNhat };
    }
    default:
      return { ...state };
  }
};
