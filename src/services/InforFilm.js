import axios from "axios";
import React from "react";
import { BASE_URL, TOKEN_CYBERSOFT } from "./configURL";

export const InforFilm = {
  getThongTinFilm: (maLichChieu) => {
    return axios.get(
      `${BASE_URL}/api/QuanLyRap/LayThongTinLichChieuPhim?MaPhim=${maLichChieu}`,
      {
        headers: {
          TokenCybersoft: TOKEN_CYBERSOFT,
        },
      }
    );
  },
};
