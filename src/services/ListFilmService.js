import axios from "axios";
import { BASE_URL, TOKEN_CYBERSOFT } from "./configURL";

export const ListFilmService = {
  getFilmSapChieu: () => {
    return axios.get(`https://6271e18625fed8fcb5ec0d34.mockapi.io/nhaKhoa`);
  },
  getFlimDangChieu: () => {
    return axios.get(`${BASE_URL}/api/QuanLyPhim/LayDanhSachPhim?maNhom=GP00`, {
      headers: {
        TokenCybersoft: TOKEN_CYBERSOFT,
      },
    });
  },
};
