import axios from "axios";
import React from "react";
import { BASE_URL, TOKEN_CYBERSOFT } from "./configURL";

export const QuanLyDatVeService = {
  getMovieTicket: (maLichChieu) => {
    return axios.get(
      `${BASE_URL}/api/QuanLyDatVe/LayDanhSachPhongVe?MaLichChieu=${maLichChieu}`,
      {
        headers: {
          TokenCybersoft: TOKEN_CYBERSOFT,
        },
      }
    );
  },

  postMovieTicKet: (thongTinDatVe) => {
    return axios({
      method: "POST",
      url: `${BASE_URL}/api/QuanLyDatVe/DatVe`,
      data: thongTinDatVe,
      headers: {
        TokenCybersoft: TOKEN_CYBERSOFT, // TokenCybersoft để cấp quền truy cập vào hệ thống
      },
    });
  },
};
