let USER = "user";
let DatVe = "DatVe";
let Phim = "Phim";
let Film = "Film";
let Banner = "Banner";

export const logcalStorageService = {
  // params để nhân thông tin từ backend
  setUserInfor: (user) => {
    // muốn lưu gì thì phải chuyển thành Json và từ Json mới  lưu đc xuống local
    let dataJson = JSON.stringify(user);
    // lưu Json
    localStorage.setItem(USER, dataJson);
  },
  // quảng cáo bannner
  setBanner: (user) => {
    // muốn lưu gì thì phải chuyển thành Json và từ Json mới  lưu đc xuống local
    let dataJson = JSON.stringify(user);
    // lưu Json
    localStorage.setItem(Banner, dataJson);
  },
  // list film đang chiếu
  setListFilm: (film) => {
    let dataJson = JSON.stringify(film);
    localStorage.setItem(Film, dataJson);
  },
  // local danh sach phim
  setDanhSachPhim: (phim) => {
    let dataJson = JSON.stringify(phim);
    localStorage.setItem(Phim, dataJson);
  },
  // local đặt vé
  setDatVeInfor: (datve) => {
    let dataJson = JSON.stringify(datve);
    localStorage.setItem(DatVe, dataJson);
  },
  getBanner: () => {
    let dataJson = localStorage.getItem(Banner);
    if (dataJson) {
      return JSON.parse(dataJson);
    } else {
      // khong co du lieu return null
      return null;
    }
  },

  // hàm lấy dữ liệu dưới local , to redux
  getUserInfor: () => {
    let dataJson = localStorage.getItem(USER);
    if (dataJson) {
      return JSON.parse(dataJson);
    } else {
      // khong co du lieu return null
      return null;
    }
  },
  getListFilmInfor: () => {
    let dataJson = localStorage.getItem(Film);
    if (dataJson) {
      return JSON.parse(dataJson);
    } else {
      // khong co du lieu return null
      return null;
    }
  },
  getDanhSachPhim: () => {
    let dataJson = localStorage.getItem(Phim);
    if (dataJson) {
      return JSON.parse(dataJson);
    } else {
      return null;
    }
  },
  getDatVeInfor: () => {
    let dataJson = localStorage.getItem(DatVe);
    if (dataJson) {
      return JSON.parse(dataJson);
    } else {
      return null;
    }
  },

  removeUserInfor: () => {
    localStorage.removeItem(USER);
  },
};
// JSON.stringify() để chuyển một Object sang JSON.
// localStorage.setItem : Với key là tên biến để ta truy xuất và value là giá trị biến truyền vào.
