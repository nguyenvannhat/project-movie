import axios from "axios";
import { BASE_URL, TOKEN_CYBERSOFT } from "./configURL";

export const userService = {
  postDangNhap: (dataLogin) => {
    return axios({
      method: "POST", // phương thức post
      url: `${BASE_URL}/api/QuanLyNguoiDung/DangNhap`, // <=> https://movienew.cybersoft.edu.vn/api/QuanLyNguoiDung/DangNhap
      data: dataLogin, // gửi thông tin đăng nhập của user lên cho backend sử lý
      headers: {
        TokenCybersoft: TOKEN_CYBERSOFT, // TokenCybersoft để cấp quền truy cập vào hệ thống
      },
    });
  },
  postDangKy: (dataRegister) => {
    return axios({
      method: "POST",
      url: `${BASE_URL}/api/QuanLyNguoiDung/DangKy`,
      data: dataRegister,
      headers: {
        TokenCybersoft: TOKEN_CYBERSOFT, // TokenCybersoft để cấp quền truy cập vào hệ thống
      },
    });
  },
 
  //
};

// nhớ có return axios
