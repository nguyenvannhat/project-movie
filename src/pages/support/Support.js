import React from "react";
import "flowbite";

export default function Support() {
  return (
    <div className="w-full px-10 h-auto phone:block ipad:flex desktop:flex pc:flex my-3 ">
      <div className="phone:w-full ipad:w-1/2 desktop:w-1/2 pc:w-1/2 phone:pr-1  ipad:pr-5 desktop:pr-5">
        <img
          src="https://cdn.chanhtuoi.com/uploads/2016/03/tong-hop-kinh-nghiem-di-xem-phim-gia-re.jpeg.webp"
          alt=""
          className="w-full"
        />
        <p className="text-justify mt-1 text-sm font-normal">
          Lorem ipsum dolor sit amet consectetur adipisicing elit. Recusandae
          repudiandae saepe suscipit? Fuga asperiores recusandae facilis error
          dolore a, suscipit adipisci fugiat hic iure neque quos ipsam earum
          provident porro numquam qui aspernatur iste nostrum, deserunt
          consequatur. Modi, blanditiis. Cumque quibusdam in suscipit amet{" "}
          <br />
          fugit, voluptate nam eveniet! Repellat repellendus fugit placeat, ipsa
          pariatur id similique at recusandae facilis vitae sed asperiores
          facere exercitationem. Odit minima quae incidunt ea numquam. Totam
          atque, similique saepe consequuntur magnam adipisci ea animi tenetur
          magni sit perferendis, autem vitae! Aliquam voluptatibus dolorum{" "}
          <br />
          obcaecati harum quisquam numquam sed, praesentium esse asperiores!
          Dolorum, inventore a. Natus, quo? Eveniet similique labore ea deleniti
          magnam commodi voluptas soluta eos maiores velit! Vel ducimus optio
          distinctio veniam error libero illo, corporis quis nam, reiciendis
          enim numquam asperiores, nobis fugit? Perspiciatis nobis ad quae
          repudiandae harum maiores, dolores vitae error quis ea, recusandae{" "}
          <br />
          velit veniam temporibus ipsa dolor dolorem eligendi, vel maxime sit
          sequi asperiores in. Molestias, animi doloremque mollitia quisquam aut
          optio et alias! Nihil quibusdam doloribus tempore dolores minima
          repellendus quas culpa laudantium. Vitae maxime ea quidem odit ipsam
          laboriosam, quaerat officiis illo asperiores deleniti porro,
          architecto odio soluta ipsa ab libero. Tempore iure numquam expedita
          quos officia.
        </p>
      </div>
      <div className=" phone:w-full ipad:w-1/2 desktop:w-1/2 pc:w-1/2  p-6 rounded-lg shadow-lg bg-white h-full text-base py-5  ">
        <h1 className="text-xl font-medium text-black">
          Bạn có gì muốn nhắn nhủ với chúng tôi nào?
        </h1>
        <h1 className="flex justify-center py-2 text-base font-medium">
          <a
            href="https://www.facebook.com/profile.php?id=100059193343778"
            className="flex mr-2"
          >
            <p className="text-orange-600 pr-2 ">Mail :</p>{" "}
            Nguyenvannhat14199@gmail.com
          </a>
        </h1>
        <form>
          <div className="form-group mb-6 ">
            <input
              type="text"
              className="form-control block h-10
      w-full
      px-3
      py-1.5
      text-base
      font-normal
      text-gray-700
      bg-white bg-clip-padding
      border border-solid border-gray-300
      rounded
      transition
      ease-in-out
      m-0
      focus:text-gray-700 focus:bg-white focus:border-blue-600 focus:outline-none"
              id="exampleInput7"
              placeholder="Name"
            />
          </div>
          <div className="form-group mb-6">
            <input
              type="email"
              className="form-control block
      w-full
      px-3
      py-1.5
      text-base
      font-normal
      text-gray-700
      bg-white bg-clip-padding
      border border-solid border-gray-300
      rounded
      transition
      ease-in-out
      m-0
      focus:text-gray-700 focus:bg-white focus:border-blue-600 focus:outline-none"
              id="exampleInput8"
              placeholder="Email address"
            />
          </div>
          <div className="form-group mb-6">
            <textarea
              className="
      form-control
      block
      w-full
      h-96
      px-3
      py-1.5
      text-base
      font-normal
      text-gray-700
      bg-white bg-clip-padding
      border border-solid border-gray-300
      rounded
      transition
      ease-in-out
      m-0
      focus:text-gray-700 focus:bg-white focus:border-blue-600 focus:outline-none
    "
              id="exampleFormControlTextarea13"
              rows={3}
              placeholder="Message"
              defaultValue={""}
            />
          </div>
          <div className="form-group form-check text-center mb-6">
            <input
              type="checkbox"
              className="form-check-input appearance-none h-4 w-4 border border-gray-300 rounded-sm bg-white checked:bg-blue-600 checked:border-blue-600 focus:outline-none transition duration-200 mt-1 align-top bg-no-repeat bg-center bg-contain mr-2 cursor-pointer"
              id="exampleCheck87"
              defaultChecked
            />
            <label
              className="form-check-label inline-block text-gray-800"
              htmlFor="exampleCheck87"
            >
              Send me a copy of this message
            </label>
          </div>
          <button
            type="submit"
            className="
    w-48
    px-6
    py-2.5
    bg-orange-600
    text-white
    font-medium
    text-xs
    leading-tight
    uppercase
    rounded
    shadow-md
    hover:bg-blue-700 hover:shadow-lg
    focus:bg-blue-700 focus:shadow-lg focus:outline-none focus:ring-0
    active:bg-blue-800 active:shadow-lg
    transition
    duration-150
    ease-in-out"
          >
            Gửi
          </button>
        </form>
      </div>
    </div>
  );
}
