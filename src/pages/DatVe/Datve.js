import React, { Fragment, useEffect, useState } from "react";
import { useDispatch, useSelector } from "react-redux";
import { Redirect, useHistory, useParams } from "react-router-dom";
import { QuanLyDatVeAction } from "../../redux/action/QuanLyDatVeAction";
import { DAT_VE } from "../../redux/constants/datVeConstants";
import { logcalStorageService } from "../../services/localStorageService";
import { QuanLyDatVeService } from "../../services/QuanLyDatVeService";
import { message } from "antd";
import "./Datve.css";

export default function Datve() {
  let USER_LOGIN = "user";
  let { id } = useParams();
  let dispatch = useDispatch();
  let history = useHistory();

  const [ticket, setticket] = useState([]);
  const [ve, setVe] = useState([]);

  const { chiTietPhongVe, danhSachGheDangDat } = useSelector(
    (state) => state.QuanLyDatVeReducer
  );

  useEffect(() => {
    QuanLyDatVeService.getMovieTicket(id)
      .then((res) => {
        setticket(res.data.content.thongTinPhim);
        setVe(res.data.content.danhSachGhe);
        logcalStorageService.setDatVeInfor(res.data.content);
        //
        dispatch(QuanLyDatVeAction(res.data.content));
      })
      .catch((err) => {
        console.log(err);
      });
  }, []);

  let renderContent = () => {
    return ve?.map((ghe, index) => {
      let classGheDangDat = "";
      let indexGheDD = danhSachGheDangDat.findIndex(
        (gheDD) => gheDD.maGhe === ghe.maGhe
      );
      if (indexGheDD != -1) {
        classGheDangDat = "gheDangDat";
      }
      if (ghe.loaiGhe === "Vip") {
        return (
          <Fragment key={index}>
            <button
              onClick={() => {
                dispatch({ type: DAT_VE, gheDuocChon: ghe });
              }}
              className={`${classGheDangDat} bg-yellow-500  w-12 h-10 m-2 rounded   `}
            >
              {ghe.tenGhe}
            </button>
            {(index + 1) % 14 === 0 ? <br /> : ""}
          </Fragment>
        );
      } else if (ghe.daDat === true) {
        return (
          <Fragment key={index}>
            <button
              disabled={ghe.daDat}
              className={`${classGheDangDat} bg-gray-500  w-12 h-10 m-2 rounded  `}
            >
              {ghe.tenGhe}
            </button>
            {(index + 1) % 14 === 0 ? <br /> : ""}
          </Fragment>
        );
      } else {
        return (
          <Fragment key={index}>
            <button
              onClick={() => {
                dispatch({ type: DAT_VE, gheDuocChon: ghe });
              }}
              className={`${classGheDangDat} bg-slate-200  w-12 h-10 m-2 rounded   `}
            >
              {ghe.tenGhe}
            </button>
            {(index + 1) % 14 === 0 ? <br /> : ""}
          </Fragment>
        );
      }
    });
  };

  if (!localStorage.getItem(USER_LOGIN)) {
    return <Redirect to="/login" />;
  }

  return (
    <div className=" py-4 phone:block ipad:flex desktop:flex pc:flex  ">
      {/* left */}
      <div className="phone:w-full ipad:w-full desktop:w-2/3 desktop:pl-5 pc:w-3/4 h-max">
        <div className="mb-4 grid    phone:grid-cols-6 phone:row-span-1 ipad:table-row-group desktop:table-row-group pc:table-row-group  ">
          {renderContent()}
        </div>
        <div className="phone:row-span-2  row row-span-1  justify-between text-base font-medium px-8   ">
          <p>
            Ghế đã được đặt{" "}
            <button className="bg-slate-600 w-12 h-10 m-2 rounded">X</button>
          </p>
          <p>
            Ghế vip{" "}
            <button className=" bg-yellow-500  w-12 h-10 m-2 rounded ">
              V
            </button>
          </p>
          <p>
            Ghế thường{" "}
            <button className="bg-slate-200 w-12 h-10 m-2 rounded">T</button>
          </p>
          <p>
            Ghế đang chọn{" "}
            <button className="bg-green-600 w-12 h-10 m-2 rounded">C</button>
          </p>
        </div>
      </div>

      {/* right */}
      <div className=" phone:w-full  ipad:w-1/3 desktop:w-1/3 pc:w-2/3 h-max shadow-2xl ">
        <ul class=" text-sm font-medium shadow ">
          <li class=" py-2 px-4  w-full h-20 rounded-t-lg border-b border-gray-400">
            <h1 className="text-2xl text-red-400">
              {" "}
              {danhSachGheDangDat
                .reduce((tongTien, ghe, index) => {
                  return (tongTien += ghe.giaVe);
                }, 0)
                .toLocaleString()}{" "}
              VND
            </h1>
          </li>
          <li class="py-2 px-4 w-full h-20 border-b border-gray-400 flex pt-4 ">
            <h1 className="w-20">Cụm rạm :</h1>
            <h1 className="pl-20">{ticket.tenCumRap}</h1>
          </li>
          <li class="py-2 px-4 w-full h-20 border-b border-gray-400 flex pt-4 ">
            <h1 className="w-20">Địa chỉ :</h1>
            <h1 className="pl-20">{ticket.diaChi}</h1>
          </li>
          <li class="py-2 px-4 w-full h-20 border-b border-gray-400 flex pt-4 ">
            <h1 className="w-20">Rạp :</h1>
            <h1 className="pl-20">{ticket.tenRap}</h1>
          </li>
          <li class="py-2 px-4 w-full h-20 border-b border-gray-400 flex pt-4 ">
            <h1 className="w-20">Ngày giờ :</h1>
            <h1 className="pl-20">{ticket.gioChieu}</h1>
          </li>
          <li class="py-2 px-4 w-full h-20 border-b border-gray-400 flex pt-4 ">
            <h1 className="w-20">Tên Phim :</h1>
            <h1 className="pl-20">{ticket.tenPhim}</h1>
          </li>
          <li class="py-2 px-4 w-full h-20 border-b  flex pt-4 ">
            <h1 className="w-20 flex text-base">ghe :</h1>
            <h1 className="pl-20">
              {danhSachGheDangDat.map((gheDD, index) => {
                return (
                  <span key={index} className=" px-1 text-green-500 text-lg">
                    {gheDD.stt}
                  </span>
                );
              })}{" "}
            </h1>
          </li>

          <button
            className="bg-red-500 w-full h-20 text-white text-base"
            onClick={() => {
              message.success("Đặt vé thành công , Check your email ");
              setInterval(() => {
                history.push("/");
              }, 3000);
            }}
          >
            ĐẶT VÉ
          </button>
        </ul>
      </div>
    </div>
    // </div>
  );
}
