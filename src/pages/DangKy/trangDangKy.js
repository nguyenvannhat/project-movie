import React from "react";
import FormDangKy from "./FormDangKy/FormDangKy";
import Lottie from "lottie-react";
import registerAnimate from "../../assets/register-animate.json";
export default function trangDangKy() {
  return (
    <div className="">
      <div className="w-full mb-4">
        <img
          src="https://www.athletadesk.com/wp-content/uploads/2016/08/join-background.jpg"
          alt=""
          className="w-full h-600 absolute "
        />

        <div className="flex w-full ">
          <div className="w-1/2   relative phone:hidden ipad:block desktop:block pc:block  ">
            <Lottie
              animationData={registerAnimate}
              className="w-620 h-auto  relative"
            />
          </div>
          <div className="w-1/2   relative  ">
            {" "}
            <FormDangKy />
          </div>
        </div>
      </div>
    </div>
  );
}
