import React from "react";
import { Button, Checkbox, Form, Input, message } from "antd";
import { userService } from "../../../services/userService";
import { useHistory } from "react-router-dom";

export default function FormDangKy() {
  let history = useHistory();
  const onFinish = (values) => {
    userService
      .postDangKy(values)
      .then((res) => {
        message.success("đăng ký thành công");
        setTimeout(() => {
          history.push("/login");
        }, 2000);
      })
      .catch((err) => {
        message.error("đăng ký thất bại");
        message.error("tài khoản đã có người đặt");
      });
  };

  const onFinishFailed = (errorInfo) => {
    message.error("đăng ký thất bại");
  };
  return (
    <Form
      className="w-96 h-auto rounded shadow-2xl  text-white mt-32  bg-white opacity-50 border-spacing-2 "
      name="basic"
      initialValues={{ remember: true }}
      onFinish={onFinish}
      onFinishFailed={onFinishFailed}
      autoComplete="off"
    >
      <div className="text-red-500 text-base font-medium"> Sign in now</div>
      <div className="w-80 ">
        <Form.Item
          label="Tài Khoản"
          name="taiKhoan"
          rules={[{ required: true, message: "Please input your username!" }]}
        >
          <Input />
        </Form.Item>
        <Form.Item
          label="Mật Khẩu"
          name="matKhau"
          rules={[{ required: true, message: "Please input your password!" }]}
        >
          <Input.Password />
        </Form.Item>
        <Form.Item
          label="Your Email"
          name="email"
          rules={[{ required: true, message: "Please input your Email!" }]}
        >
          <Input />
        </Form.Item>
        <Form.Item
          label="Your phone"
          name="soDt"
          rules={[
            { required: true, message: "PPlease input your phone number!" },
          ]}
        >
          <Input />
        </Form.Item>

        <Form.Item
          label=" Họ  tên     "
          name="hoTen"
          rules={[{ required: true, message: "Please input your name!" }]}
        >
          <Input />
        </Form.Item>

        <Form.Item wrapperCol={{ offset: 8, span: 16 }}>
          <Button className="bg-blue-500 text-white" htmlType="submit">
            Register
          </Button>
        </Form.Item>
      </div>
    </Form>
  );
}
