import React from "react";

export default function GiaVe() {
  return (
    <div>
      <div className="w-full phone:block ipad:flex desktop:flex pc:flex px-5">
        <div className="w-1/2 pr-4 ">
          <h2 className="font-medium text-xl text-left py-2">
            Galaxy Buôn Ma Thuật
          </h2>
          <div className="grid grid-cols-1 gap-5">
            <div>
              <img
                className="w-3/2 h-80"
                src="https://cdn.galaxycine.vn/media/2019/12/11/galaxy7_1576054843437.jpg"
                alt=""
              />
              <h3
                className="font-normal text-base text-left justify-between "
                style={{ width: "480px" }}
              >
                Rạp chiếu phim Galaxy Buôn Ma Thuột là cụm rạp thứ 3 tại khu
                vực, chính thức đi vào hoạt động từ tháng 7/2019. Với hệ thống
                trang thiết bị được đầu tư hiện đại, cùng với đội ngũ nhân viên
                chuyên nghiệp, Galaxy Buôn Ma Thuột tự tin mang đến cho khán giả
                những trải nghiệm điện ảnh hoàn hảo nhất.
              </h3>
            </div>
            <div>
              <img
                className="w-3/2 h-80"
                src="https://cdn.galaxycine.vn/media/2019/12/11/galaxy40_1576054846557.jpg"
                alt=""
              />
              <h3
                className="font-normal text-base text-left justify-between "
                style={{ width: "480px" }}
              >
                Rạp Galaxy Buôn Ma Thuột hiện có tất cả 5 phòng chiếu, bao gồm
                cả định dạng 2D và 3D nhằm đáp ứng nhu cầu thưởng thức phim đa
                dạng của khán giả phố núi. Mỗi phòng chiếu đều được trang bị màn
                hình kích thước lớn có độ sắc nét cao, âm thanh chuẩn Dolby 7.1
                và hệ thống ghế ngồi cao cấp, đảm bảo khán giả luôn cảm thấy
                thoải mái khi xem phim trong khoảng thời gian khá dài trong rạp.
              </h3>
            </div>
            <div>
              <img
                className="w-3/2 h-80"
                src="https://cdn.galaxycine.vn/media/2019/12/11/galaxy46_1576054852883.jpg"
                alt=""
              />
              <h3
                className="font-normal text-base text-left justify-between "
                style={{ width: "480px" }}
              >
                Không gian của Galaxy Buôn Ma Thuột được thiết kế theo phong
                cách hiện đại, với những điểm nhấn ấn tượng và thường xuyên được
                thay đổi phong cách trang trí vào những dịp đặc biệt. Do vậy nên
                khán giả đến rạp Galaxy Buôn Ma Thuột xem phim luôn “thu hoạch”
                được những bức hình cực chill. <br />
                Đội ngũ nhân viên của Galaxy Buôn Ma Thuột được đào tạo chuyên
                nghiệp nhằm mang đến chất lượng phục vụ chu đáo và chất lượng
                nhất. Vậy nên sau một thời gian đi vào hoạt động, Galaxy Buôn Ma
                Thuột đã trở thành địa điểm giải trí quen thuộc được yêu thích
                của khán giả Đắk Lắk.
              </h3>
            </div>
          </div>
        </div>
        <div className="w-1/2 ">
          <h2 className="font-medium text-xl text-left my-2">Giá vé</h2>
          <ul
            class="nav nav-tabs flex flex-col md:flex-row flex-wrap list-none border-b-0 pl-0 mb-4"
            id="tabs-tab"
            role="tablist"
          >
            <li class="nav-item" role="presentation">
              <a
                href="#tabs-home"
                class="
      nav-link
      block
      font-medium
      text-xs
      leading-tight
      uppercase
      border-x-0 border-t-0 border-b-2 border-transparent
      px-6
      py-3
      my-2
      hover:border-transparent hover:bg-orange-500
      focus:border-transparent
      active
    "
                id="tabs-home-tab"
                data-bs-toggle="pill"
                data-bs-target="#tabs-home"
                role="tab"
                aria-controls="tabs-home"
                aria-selected="true"
              >
                2D
              </a>
            </li>

            <li class="nav-item" role="presentation">
              <a
                href=""
                class="
      nav-link
      block
      font-medium
      text-xs
      leading-tight
      uppercase
      border-x-0 border-t-0 border-b-2 border-transparent
      px-6
      py-3
      my-2
      hover:border-transparent hover:bg-orange-500
      focus:border-transparent
    "
                id="tabs-messages-tab"
                data-bs-toggle="pill"
                data-bs-target="#tabs-messages"
                role="tab"
                aria-controls="tabs-messages"
                aria-selected="false"
              >
                3D
              </a>
            </li>
          </ul>
          <div class="tab-content" id="tabs-tabContent">
            <div
              class="tab-pane fade show active"
              id="tabs-home"
              role="tabpanel"
              aria-labelledby="tabs-home-tab"
            >
              <img
                src="https://cdn.galaxycine.vn/media/2022/4/8/banggiave-camau-040422-2d-web_1649418339390.jpg"
                alt=""
              />
            </div>

            <div
              class="tab-pane fade"
              id="tabs-messages"
              role="tabpanel"
              aria-labelledby="tabs-profile-tab"
            >
              <img
                src="https://cdn.galaxycine.vn/media/2019/8/30/banggiave-082019-3d_1567135215938.jpg"
                alt=""
                style={{ width: "600px" }}
              />
            </div>
          </div>
        </div>
      </div>
    </div>
  );
}
