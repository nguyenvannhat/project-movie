import React, { useEffect, useState } from "react";
import { PromotionService } from "../../../services/PromotionService";
import AboutUs from "../aboutUs/AboutUs";
import Promotion from "./Promotion";

export default function ChiTiet() {
  const [promotion, setPromotion] = useState([]);
  useEffect(() => {
    PromotionService.getPromotionService()
      .then((res) => {
        console.log("Promotion :", res);
        setPromotion(res.data);
      })
      .catch((err) => {
        console.log(err);
      });
  }, []);
  let render = () => {
    return promotion.map((item) => {
      return (
        <div class="flex justify-center">
          <div class="flex flex-col md:flex-row md:max-w-xl rounded-lg bg-white shadow-lg">
            <img
              class=" w-full h-96 md:h-auto object-cover md:w-48 rounded-t-lg md:rounded-none md:rounded-l-lg"
              src={item.image}
              alt=""
            />
            <div class="p-6 flex flex-col justify-start">
              <h5 class="text-gray-900 text-xl font-medium mb-2">
                {item.title}
              </h5>
              <p class="text-gray-700 text-base mb-4">{item.one}</p>
              <p class="text-gray-600 text-xs">{item.two}</p>
            </div>
          </div>
        </div>
      );
    });
  };
  return (
    <div className="w-full px-5 mt-2">
      <div className="grid grid-cols-2 gap-3">{render()}</div>
    </div>
  );
}
