import React, { Fragment, useEffect, useState } from "react";
import { PromotionService } from "../../../services/PromotionService";
import ChiTiet from "./ChiTiet";

export default function Promotion() {
  const [promotion, setPromotion] = useState([]);
  useEffect(() => {
    PromotionService.getPromotionService()
      .then((res) => {
        console.log("Promotion :", res);
        setPromotion(res.data);
      })
      .catch((err) => {
        console.log(err);
      });
  }, []);

  let renderContent = () => {
    return promotion.map((item) => {
      return (
        <div class="">
          <div class="relative overflow-hidden bg-no-repeat bg-cover ">
            <img
              src={item.image}
              class="w-full phone:h-72 ipad:h-429 desktop:h-429 pc:h-429"
            />
            <div class="absolute bg-cover top-0 right-0 bottom-0 left-0  overflow-hidden bg-fixed opacity-0 hover:opacity-70 transition duration-300 ease-in-out bg-black">
              <a href="/khuyenMai">
                <h2 className="text-white text-xl font-normal mt-4">
                  {item.title}
                </h2>
                <button
                  type="button"
                  class="inline-block px-6 py-2 items-end phone:mt-28 ipad:mt-64 desktop:mt-72 pc:mt-72 text-white border-2 border-white text-base font-light leading-tight uppercase  hover:bg-yellow-600 hover:bg-opacity-9 focus:outline-none focus:ring-0 transition duration-150 ease-in-out"
                >
                  Xem chi Tiết
                </button>
              </a>
            </div>
          </div>
        </div>
      );
    });
  };
  return (
    <div className=" ">
      <h1 className="text-2xl text-black py-3  text-left">
        Tin Khuyến Mãi
        <hr className="bg-orange-500 w-48" />{" "}
      </h1>
      <div class="grid phone:grid-cols-2 ipad:grid-cols-3 desktop:grid-cols-4 pc:grid-cols-5 gap-10">
        {renderContent()}
      </div>
    </div>
  );
}
