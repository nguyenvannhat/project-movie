import React, { Fragment } from "react";

export default function AboutUs() {
  return (
    <div className=" mb-5">
      <h1 className="text-2xl text-black py-3  text-left">
        Về chúng tôi <hr className="bg-orange-500 w-36" />{" "}
      </h1>
      <div className=" ">
        <h1 className="text-justify text-base ">
          <b className="text-base font-medium">NVN Cinema</b> là một trong những
          công ty tư nhân đầu tiên về điện ảnh được thành lập từ năm 2003, đã
          khẳng định thương hiệu là 1 trong 10 địa điểm vui chơi giải trí được
          yêu thích nhất. Ngoài hệ thống rạp chiếu phim hiện đại, thu hút hàng
          triệu lượt người đến xem,{" "}
          <b className="text-base font-medium">NVN Cinema</b> còn hấp dẫn khán
          giả bởi không khí thân thiện cũng như chất lượng dịch vụ hàng đầu.{" "}
          <br /> Đến website <b className="text-base font-medium">NVN Cinema</b>
          , khách hàng sẽ dễ dàng tham khảo các phim hay nhất, phim mới nhất
          đang chiếu hoặc sắp chiếu luôn được cập nhật thường xuyên. Lịch chiếu
          tại tất cả hệ thống rạp chiếu phim của Galaxy Cinema cũng được cập
          nhật đầy đủ hàng ngày hàng giờ trên trang chủ. Từ vũ trụ điện ảnh
          Marvel, người hâm mộ <br />
          sẽ có cuộc tái ngộ với Người Nhện qua Spider-Man: No Way Home hoặc
          Doctor Strange 2. Ngoài ra 007: No Time To Die, Turning Red, Minions:
          The Rise Of Gru..., là những tác phẩm hứa hẹn sẽ gây bùng nổ phòng vé{" "}
          <br />
          trong thời gian tới. Giờ đây đặt vé tại{" "}
          <b className="text-base font-medium">NVN Cinema</b> càng thêm dễ dàng
          chỉ với vài thao tác vô cùng đơn giản. Để mua vé, hãy vào tab Mua vé.
          Quý khách có thể chọn Mua vé theo phim, theo rạp, hoặc theo ngày. Sau
          đó, tiến hành mua vé theo các bước hướng dẫn. Chỉ trong vài phút, quý
          khách sẽ nhận được tin nhắn và email phản hồi Đặt vé thành công <br />
          của Galaxy Cinema. Quý khách có thể dùng tin nhắn lấy vé tại quầy vé
          của Galaxy Cinema hoặc quét mã QR để một bước vào rạp mà không cần tốn
          thêm bất kỳ công đoạn nào nữa. Nếu bạn đã chọn được phim hay để xem,
          hãy đặt vé cực nhanh bằng box Mua Vé Nhanh ngay từ Trang Chủ. Chỉ cần
          một phút, tin nhắn và email phản hồi của{" "}
          <b className="text-base font-medium">NVN Cinema</b> sẽ gửi ngay vào
          điện thoại và hộp mail của bạn. Nếu chưa quyết định sẽ xem phim mới{" "}
          <br />
          nào, hãy tham khảo các bộ phim hay trong mục Phim Đang Chiếu cũng như
          Phim Sắp Chiếu tại rạp chiếu phim bằng cách vào mục Bình Luận Phim ở
          Góc Điện Ảnh để đọc những bài bình luận chân thật nhất, tham khảo và
          cân nhắc. Sau đó, chỉ việc đặt vé bằng box Mua Vé Nhanh ngay ở đầu
          trang để chọn được suất chiếu và chỗ ngồi vừa ý nhất.{" "}
          <b className="text-base font-medium">NVN Cinema</b>
          luôn có những chương trình khuyến mãi, ưu đãi, quà tặng vô cùng hấp
          dẫn như giảm giá vé, tặng vé xem phim miễn phí, tặng Combo, tặng quà
          phim… dành cho các khách hàng. Trang web{" "}
          <b className="text-base font-medium">NVNCinema.vn</b> còn có mục Góc
          Điện Ảnh - nơi lưu trữ dữ liệu về phim, diễn viên và đạo diễn, những{" "}
          <br />
          bài viết chuyên sâu về điện ảnh, hỗ trợ người yêu phim dễ dàng hơn
          trong việc lựa chọn phim và bổ sung thêm kiến thức về điện ảnh cho bản
          thân. Ngoài ra, vào mỗi tháng,{" "}
          <b className="text-base font-medium">NVN Cinema</b> cũng giới thiệu
          các phim sắp chiếu hot nhất trong mục Phim Hay Tháng . Hiện nay,
          <b className="text-base font-medium">NVN Cinema</b> <br />
          đang ngày càng phát triển hơn nữa với các chương trình đặc sắc, các
          khuyến mãi hấp dẫn, đem đến cho khán giả những bộ phim bom tấn của thế
          giới và Việt Nam nhanh chóng và sớm nhất.
          <br />
        </h1>
      </div>
    </div>
  );
}
