import { Carousel } from "antd";
import MovieItem from "../MovieItem/MovieItem";
const contentStyle = {
  height: "260px",
  color: "#FF0000",
  lineHeight: "260px",
  textAlign: "center",
  background: "#FF0000",
};

const MovieCarousel = ({ chunkedList }) => {
  const onChange = (currentSlide) => {};

  // 1. map ra 4 cai list : chunked 4 item ra 3 list
  // 2. map ra từng item
  return (
    <Carousel afterChange={onChange} autoplay>
      {chunkedList.map((movies, index) => {
        return (
          <div style={contentStyle} className="h-max w-full py-10">
            <div className="grid phone:grid-cols-2 ipad:grid-cols-2 desktop:grid-cols-4 pc:grid-cols-5 gap-5">
              {movies.map((item) => {
                return <MovieItem movie={item} style={contentStyle} />;
              })}
            </div>
          </div>
        );
      })}
    </Carousel>
  );
};

export default MovieCarousel;
