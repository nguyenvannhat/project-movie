import React from "react";

import { Card, Button } from "antd";
import { NavLink } from "react-router-dom";
import moment from "moment";
const { Meta } = Card;

const MovieItem = ({ movie }) => (
  <div className="max-w-sm bg-white rounded-lg border border-gray-200 shadow-md dark:bg-gray-800 dark:border-gray-700">
    <img
      className="rounded-t-lg w-full phone:h-40 ipad:h-72 desktop:h-80 pc:h-96"
      src={movie.hinhAnh}
      alt
    />

    <div className=" phone:block pc:px-5">
      <p className="mb-3 mt-2 font-medium text-base  text-gray-900 dark:text-gray-800">
        {movie.tenPhim.slice(0, 20)} ...
      </p>
      <NavLink to={`/detail/${movie.maPhim}`}>
        <button class="inline-flex items-center  bg-cyan-600 hover:bg-blue-700 text-white font-bold py-2 px-4 rounded mb-2">
          Xem Thêm
          <svg
            aria-hidden="true"
            className="ml-1 w-4 h-4"
            fill="currentColor"
            viewBox="0 0 20 20"
            xmlns="http://www.w3.org/2000/svg"
          >
            <path
              fillRule="evenodd"
              d="M10.293 3.293a1 1 0 011.414 0l6 6a1 1 0 010 1.414l-6 6a1 1 0 01-1.414-1.414L14.586 11H3a1 1 0 110-2h11.586l-4.293-4.293a1 1 0 010-1.414z"
              clipRule="evenodd"
            />
          </svg>
        </button>
      </NavLink>
    </div>
  </div>
);
// khi click xem chi tiết thì sẽ chuyển qua trang xem chi tiết và gửi kèm 1 mã phim dùng mã phim đi gọi api
export default MovieItem;
