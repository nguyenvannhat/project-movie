import moment from "moment";
import React from "react";
import { Fragment } from "react";
import { NavLink } from "react-router-dom";

export default function MovieTabItem({ movie, index }) {
  // console.log("movie", movie);

  return (
    <Fragment key={index}>
      <div className="my-2 flex">
        <div className=" phone:block ipad:flex desktop:flex pc:flex">
          <img
            className=" phone:w-20 phone:h-20 ipad:w-24 ipad:h-24 desktop:w-28 desktop:h-28 pc:w-32 pc:h-32"
            src={movie.hinhAnh}
            alt=""
          />
          <div className="phone:block ipad:ml-2 desktop:ml-5 pc:ml-6">
            <h2 className=" whitespace-normal desktop:ml-2 pc:ml-3 font-medium text-base text-blue-900 text-left">
              {movie.tenPhim}
            </h2>
            <div className="grid phone:grid-cols-1 ipad:grid-cols-2 desktop:grid-cols-3 pc:grid-cols-4 gap-2">
              {movie.lstLichChieuTheoPhim
                ?.slice(0, 5)
                .map((lichChieu, index) => {
                  return (
                    <NavLink
                      className="text-base phone:block ipad:block desktop:mx-2 pc:mx-3 mt-2"
                      to={`/datVe/${lichChieu.maLichChieu}`}
                    >
                      <button
                        type="button"
                        class="text-white bg-gradient-to-r from-cyan-500 to-blue-500 hover:bg-gradient-to-bl focus:ring-4 focus:outline-none focus:ring-cyan-300 dark:focus:ring-cyan-800 font-medium rounded-lg text-sm
                       phone:px-2 ipad:px-3  desktop:px-5 pc:px-7 py-2 text-center mr-2 mb-2"
                      >
                        <p>
                          {moment(lichChieu.ngayChieuGioChieu).format(
                            "DD/MM/YYYY"
                          )}
                        </p>
                      </button>
                    </NavLink>
                  );
                })}
            </div>
          </div>
        </div>
      </div>
      <hr />
    </Fragment>
  );
}
