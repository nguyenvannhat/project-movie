import { Tabs } from "antd";
import React, { useEffect, useState } from "react";
import { MovieService } from "../../../services/MovieService";
import MovieTabItem from "./MovieTabItem";
const { TabPane } = Tabs;

const onChange = (key) => {};

export default function MovieTabs() {
  const [dataRaw, setDataRaw] = useState([]);
  useEffect(() => {
    MovieService.getMovieByTheater()
      .then((res) => {
        setDataRaw(res.data.content);
      })
      .catch((err) => {});
  }, []);

  let renderContent = () => {
    return dataRaw.map((heThongRap, index) => {
      // index == key của tabPanne
      // tabPane trả về Img lần map đầu tiên
      // tabs map lan 2
      return (
        <TabPane
          tab={<img src={heThongRap.logo} className="w-10 h-10" />}
          key={index}
        >
          <Tabs className="h-500" tabPosition="left" defaultActiveKey="1">
            {heThongRap.lstCumRap.map((cumRap, index) => {
              return (
                <TabPane
                  tab={
                    <div className="w-80 flex">
                      <div className="text-left ">
                        <p className="text-green-400 font-medium text-base">
                          {cumRap.tenCumRap}
                        </p>
                        <p className="font-medium">{cumRap.diaChi}</p>
                        <p className="text-red-400">[Chi tiết]</p>
                        {/* <img src={cumRap.hinhAnh} alt="" /> */}
                      </div>
                    </div>
                  }
                  key={index}
                >
                  <div className="snap-y h-500 overflow-y-scroll">
                    {cumRap.danhSachPhim.map((phim, index) => {
                      // chỉ trả về < 20 item
                      if (index < 20) {
                        {
                          return <MovieTabItem key={index} movie={phim} />;
                        }
                      }
                    })}
                  </div>
                </TabPane>
              );
            })}
          </Tabs>
        </TabPane>
      );
    });
  };
  let renderContentPhone = () => {
    return dataRaw.map((heThongRap, index) => {
      // index == key của tabPanne
      // tabPane trả về Img lần map đầu tiên
      // tabs map lan 2
      return (
        <TabPane
          tab={<img src={heThongRap.logo} className="w-10 h-7  " />}
          key={index}
        >
          <Tabs className="h-500  " tabPosition="left" defaultActiveKey="1">
            {heThongRap.lstCumRap.map((cumRap, index) => {
              return (
                <TabPane
                  tab={
                    <div className="w-28 flex whitespace-normal text-center">
                      <div className="text-left ">
                        <p className="text-green-400 font-medium text-base">
                          {cumRap.tenCumRap}
                        </p>
                        <p className="font-medium">{cumRap.diaChi}</p>
                        <p className="text-red-400">[Chi tiết]</p>
                        {/* <img src={cumRap.hinhAnh} alt="" /> */}
                      </div>
                    </div>
                  }
                  key={index}
                >
                  <div className="snap-y h-500 overflow-y-scroll">
                    {cumRap.danhSachPhim.map((phim, index) => {
                      // chỉ trả về < 20 item
                      if (index < 20) {
                        {
                          return <MovieTabItem key={index} movie={phim} />;
                        }
                      }
                    })}
                  </div>
                </TabPane>
              );
            })}
          </Tabs>
        </TabPane>
      );
    });
  };
  return (
    <div className="mt-5">
      <div className="phone:hidden ipad:block  desktop:block pc:block shadow-xl rounded-md ">
        <Tabs
          tabPosition="left"
          defaultActiveKey="1"
          onChange={onChange}
          className="    desktop:h-500  pc:h-600"
        >
          {renderContent()}
        </Tabs>
      </div>
      <div className="phone:block ipad:hidden  desktop:hidden pc:hidden">
        <Tabs
          tabPosition="top"
          defaultActiveKey="1"
          onChange={onChange}
          className="  ipad:h-500  desktop:h-500  pc:h-600 shadow-2xl"
        >
          {renderContentPhone()}
        </Tabs>
      </div>
    </div>
  );
}
