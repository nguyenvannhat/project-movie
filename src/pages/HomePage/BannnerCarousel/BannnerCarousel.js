import { Carousel } from "antd";
const contentStyle = {
  height: "260px",
  color: "#ffff",
  lineHeight: "160px",
  textAlign: "center",
  background: "black",
};

const BannnerCarousel = ({ bannerList }) => {
  const onChange = (currentSlide) => {};

  return (
    <Carousel afterChange={onChange} autoplay>
      {bannerList.map((item) => {
        return (
          <div className="">
            <img
              src={item.image}
              alt=""
              className=" phone:h-64 ipad:h-429 desktop:w-full desktop:h-600 pc:h-610 "
            />
          </div>
        );
      })}
    </Carousel>
  );
};

export default BannnerCarousel;
