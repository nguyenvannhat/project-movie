import React from "react";

const MovieSlection = () => {
  return (
    <div className="justify-center pt-2 phone:hidden ipad:w-auto desktop:w-full pc:w-full">
      =============================================================================================
      <div className="  ">
        <img
          src="https://www.cgv.vn/skin/frontend/cgv/default/images/bg-cgv/h3_movie_selection.gif"
          alt=""
          className="inline-block "
        />
      </div>
      ============================================================================
    </div>
  );
};

export default MovieSlection;
