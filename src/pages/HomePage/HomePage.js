import _ from "lodash";
import React, { useEffect, useState } from "react";
import { MovieService } from "../../services/MovieService";
import MovieCarousel from "./Carousel/MovieCarousel";
import { BannerService } from "../../services/BannerService";
import BannnerCarousel from "./BannnerCarousel/BannnerCarousel";
import "./HomePage.css";

import MovieSlection from "./MovieSelection/MovieSlection";
import MovieTabs from "./MovieTabs/MovieTabs";
import Promotion from "./promotion/Promotion";
import { logcalStorageService } from "../../services/localStorageService";
import { useDispatch } from "react-redux";

import QuanLyPhimAction from "../../redux/action/QuanLyPhimAction";
import AboutUs from "./aboutUs/AboutUs";
import { QuanLyBannerReducer } from "../../redux/reducers/QuanLyBannerReducer";
import { QuanLyBannerAction } from "../../redux/action/QuanLyBannerAction";
// HomePage trang chính
export default function HomePage() {
  let dispatch = useDispatch();
  const [movieList, setMovieList] = useState([]);
  console.log("movieList", movieList);
  useEffect(() => {
    // let fetchMoviesList = async () => {
    //   let result = await MovieService.getMovieList();
    //   let chunkedList = _.chunk(result.data.content, 8);
    //   setMovieList(chunkedList);
    // };
    MovieService.getMovieList()
      .then((res) => {
        let chunkedList = _.chunk(res.data.content, 8);
        setMovieList(chunkedList);
        logcalStorageService.setDanhSachPhim(res.data.content);
        dispatch(QuanLyPhimAction(res.data.content));
      })
      .catch((err) => {
        console.log(err);
      });
  }, []);

  const [bannerList, setBannerList] = useState([]);

  useEffect(() => {
    BannerService.postLayDanhSachBanner()
      .then((res) => {
        setBannerList(res.data);
        logcalStorageService.setBanner(res.data);
        dispatch(QuanLyBannerAction(res.data));
      })
      .catch((err) => {
        console.log(err);
      });
  }, []);
  return (
    <div className="w-full ">
      <div className=" ">
        <BannnerCarousel bannerList={bannerList} />
      </div>
      <div className="pt-2 phone:block ipad:px-8 desktop:px-10 pc:px-12">
        <MovieSlection />
      </div>
      <div className="carousel_homepage container  phone:px-2 ipad:px-8 desktop:px-10 pc:px-12">
        <MovieCarousel chunkedList={movieList} />
      </div>
      <div className="container phone:px-2 ipad:px-8 desktop:px-10 pc:px-12">
        <MovieTabs className="" />
      </div>
      <div className="container px-10 ">
        <Promotion />
      </div>
      <div className="container px-10">
        <AboutUs />
      </div>
    </div>
  );
}

// dùng lodash để phân trang : npm i lodash
