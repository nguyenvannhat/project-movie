import { Carousel } from "antd";
import _ from "lodash";
import React, { useEffect, useState } from "react";
import { useDispatch } from "react-redux";
import { NavLink } from "react-router-dom";
import { QuanLyListFilmAction } from "../../redux/action/QuanLyListFilmAction";
import { ListFilmService } from "../../services/ListFilmService";
import { logcalStorageService } from "../../services/localStorageService";
const contentStyle = {
  height: "260px",
  color: "#FF0000",
  lineHeight: "260px",
  textAlign: "center",
  background: "#FF0000",
};

export default function PhimDangKhoiChieu() {
  let dispatch = useDispatch();
  const [state, setstate] = useState([]);
  useEffect(() => {
    ListFilmService.getFlimDangChieu()
      .then((res) => {
        let chunkedList = _.chunk(res.data.content, 8);
        setstate(chunkedList);
        logcalStorageService.setListFilm(res.data.content);
        dispatch(QuanLyListFilmAction(res.data.content));
      })
      .catch((err) => {});
  }, []);
  let renderContent = () => {
    return state.map((movie) => {
      return (
        <div className="h-max w-full py-10">
          <div className="grid phone:grid-cols-2 ipad:grid-cols-3 desktop:grid-cols-4 pc:grid-cols-5 gap-5">
            {movie.map((item) => {
              return (
                <div class="flex justify-center">
                  <div class="rounded-lg shadow-lg bg-white w-96">
                    <div class="relative overflow-hidden bg-no-repeat bg-cover max-w-xs">
                      <img
                        src={item.hinhAnh}
                        class="rounded-t-lg w-full phone:h-40 ipad:h-72 desktop:h-80 pc:h-96"
                        alt="Louvre"
                      />
                      <div class="absolute top-0 right-0 bottom-0 left-0 w-full h-full overflow-hidden bg-fixed opacity-0 hover:opacity-40 transition duration-300 ease-in-out bg-white"></div>
                    </div>

                    <div class="phone:block pc:px-5">
                      <p className="mb-3 mt-2 font-medium text-base  text-gray-900 dark:text-gray-800">
                        {item.tenPhim.slice(0, 20)}...
                      </p>

                      <NavLink to={`/detail/${item.maPhim}`}>
                        <button
                          type="button"
                          class="inline-flex items-center  bg-cyan-600 hover:bg-blue-700 text-white font-bold py-2 px-4 rounded mb-2"
                        >
                          Xem chi tiết
                          <svg
                            aria-hidden="true"
                            className="ml-1 w-4 h-4"
                            fill="currentColor"
                            viewBox="0 0 20 20"
                            xmlns="http://www.w3.org/2000/svg"
                          >
                            <path
                              fillRule="evenodd"
                              d="M10.293 3.293a1 1 0 011.414 0l6 6a1 1 0 010 1.414l-6 6a1 1 0 01-1.414-1.414L14.586 11H3a1 1 0 110-2h11.586l-4.293-4.293a1 1 0 010-1.414z"
                              clipRule="evenodd"
                            />
                          </svg>
                        </button>
                      </NavLink>
                    </div>
                  </div>
                </div>
              );
            })}
          </div>
        </div>
      );
    });
  };
  return (
    <div className="carousel_homepage container mx-auto">
      <div className="w-full h-max py-5 ">
        <div>
          <Carousel autoplay>{renderContent()}</Carousel>
        </div>
      </div>
    </div>
  );
}
