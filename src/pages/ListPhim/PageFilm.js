import React from "react";
import PhimDangKhoiChieu from "./PhimDangKhoiChieu";

import PhimSapRaMat from "./PhimSapRaMat";

export default function PageFilm() {
  return (
    <div>
      <div className="w-full px-5 h-max">
        <h2 className="font-medium text-xl text-left my-2">Phim Hot</h2>
        <ul
          class="nav nav-tabs flex flex-col md:flex-row flex-wrap list-none border-b-0 pl-0 mb-4"
          id="tabs-tab"
          role="tablist"
        >
          <li class="nav-item" role="presentation">
            <a
              href="#tabs-home"
              class="
      nav-link
      block
      font-medium
      text-xs
      leading-tight
      uppercase
      border-x-0 border-t-0 border-b-2 border-transparent
      px-6
      py-3
      my-2
      hover:border-transparent hover:bg-orange-500
      focus:border-transparent
      active
    "
              id="tabs-home-tab"
              data-bs-toggle="pill"
              data-bs-target="#tabs-home"
              role="tab"
              aria-controls="tabs-home"
              aria-selected="true"
            >
              Phim sắp công chiếu
            </a>
          </li>

          <li class="nav-item" role="presentation">
            <a
              href="#tabs-messages"
              class="
      nav-link
      block
      font-medium
      text-xs
      leading-tight
      uppercase
      border-x-0 border-t-0 border-b-2 border-transparent
      px-6
      py-3
      my-2
      hover:border-transparent hover:bg-orange-500
      focus:border-transparent
    "
              id="tabs-messages-tab"
              data-bs-toggle="pill"
              data-bs-target="#tabs-messages"
              role="tab"
              aria-controls="tabs-messages"
              aria-selected="false"
            >
              Phim đang chiếu
            </a>
          </li>
        </ul>
        <div class="tab-content" id="tabs-tabContent">
          <div
            class="tab-pane fade show active"
            id="tabs-home"
            role="tabpanel"
            aria-labelledby="tabs-home-tab"
          >
            <PhimSapRaMat />
          </div>

          <div
            class="tab-pane fade"
            id="tabs-messages"
            role="tabpanel"
            aria-labelledby="tabs-profile-tab"
          >
            <PhimDangKhoiChieu />
          </div>
        </div>
      </div>
    </div>
  );
}
