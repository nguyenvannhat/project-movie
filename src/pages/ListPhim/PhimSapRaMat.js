import React, { useEffect, useState } from "react";
import { ListFilmService } from "../../services/ListFilmService";
import _ from "lodash";
import { Carousel } from "antd";
import { NavLink } from "react-router-dom";
import { useDispatch } from "react-redux";
import { logcalStorageService } from "../../services/localStorageService";
import { QuanLyListFilmAction } from "../../redux/action/QuanLyListFilmAction";
export default function PhimSapRaMat() {
  let dispatch = useDispatch();
  const [film, setFilm] = useState([]);
  useEffect(() => {
    ListFilmService.getFilmSapChieu()
      .then((res) => {
        // console.log("phim sap chieu", res);
        // let chunkedList = _.chunk(res, 4);
        setFilm(res.data);
        logcalStorageService.setListFilm(res.data);
        dispatch(QuanLyListFilmAction(res.data));
      })
      .catch((err) => {
        // console.log(err);
      });
  }, []);
  let renderContent = () => {
    return film.map((item) => {
      return (
        <div>
          <div class="flex justify-center">
            <div class="rounded-lg shadow-lg bg-white max-w-sm">
              <img
                class="rounded-t-lg w-full phone:h-56 ipad:h-64 desktop:h-80 pc:h-96"
                src={item.image}
                alt=""
              />
              ;
              <div class="p-6">
                <h5 class="text-gray-900 text-xl font-medium mb-2">
                  {/* {movie.tenPhim.slice(0, 20)} */}
                </h5>
                <p class="text-gray-700 text-base mb-4 text-left phone:hidden ipad:hidden desktop:block pc:block ">
                  Bộ phim mô tả : {item.description.slice(0, 120)} ...
                </p>
                <p class="text-gray-700 text-base mb-4 text-left phone:block ipad:block desktop:hidden pc:hidden">
                  Bộ phim mô tả : {item.description.slice(0, 20)} ...
                </p>
                {/* start trailer */}
                <button
                  type="button"
                  class="inline-block px-6 py-2.5 bg-blue-500 text-white font-medium text-xs leading-tight uppercase rounded shadow-md hover:bg-yellow-500 hover:shadow-lg focus:bg-blue-700 focus:shadow-lg focus:outline-none focus:ring-0 active:bg-blue-800 active:shadow-lg transition duration-150 ease-in-out"
                  data-bs-toggle="modal"
                  data-bs-target="#exampleModalLg"
                >
                  Trailer
                </button>
                <div
                  class="modal fade fixed top-0 left-0 hidden w-full h-full outline-none overflow-x-hidden overflow-y-auto"
                  id="exampleModalLg"
                  tabindex="-1"
                  aria-labelledby="exampleModalLgLabel"
                  aria-modal="true"
                  role="dialog"
                >
                  <div class="modal-dialog modal-lg relative w-auto pointer-events-none">
                    <div class="modal-content border-none shadow-lg relative flex flex-col w-full pointer-events-auto bg-white bg-clip-padding rounded-md outline-none text-current">
                      <div class="modal-header flex flex-shrink-0 items-center justify-between p-4 border-b border-gray-200 rounded-t-md">
                        <h5
                          class="text-xl font-medium leading-normal text-gray-800"
                          id="exampleModalLgLabel"
                        >
                          {item.name}
                        </h5>
                        <button
                          type="button"
                          class="btn-close box-content w-4 h-4 p-1 text-black border-none rounded-none opacity-50 focus:shadow-none focus:outline-none focus:opacity-100 hover:text-black hover:opacity-75 hover:no-underline"
                          data-bs-dismiss="modal"
                          aria-label="Close"
                        ></button>
                      </div>
                      <div class="  p-4">
                        <iframe
                          className=" phone:w-80 phone:h-96 ipad:w-96 ipad:h-429 desktop:w-760 desktop:h-500  pc:w-760 pc:h-500 "
                          src={item.youtube}
                          title="YouTube video player"
                          frameborder="0"
                          allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture"
                          allowfullscreen
                        >
                          a
                        </iframe>
                      </div>
                      <div class="modal-footer flex flex-shrink-0 flex-wrap items-center justify-end p-4 border-t border-gray-200 rounded-b-md">
                        <button
                          type="button"
                          class="inline-block px-6 py-2.5 bg-purple-600 text-white font-medium text-xs leading-tight uppercase rounded shadow-md hover:bg-purple-700 hover:shadow-lg focus:bg-purple-700 focus:shadow-lg focus:outline-none focus:ring-0 active:bg-purple-800 active:shadow-lg transition duration-150 ease-in-out"
                          data-bs-dismiss="modal"
                        >
                          Close
                        </button>
                        <button
                          type="button"
                          class="inline-block px-6 py-2.5 bg-blue-600 text-white font-medium text-xs leading-tight uppercase rounded shadow-md hover:bg-blue-700 hover:shadow-lg focus:bg-blue-700 focus:shadow-lg focus:outline-none focus:ring-0 active:bg-blue-800 active:shadow-lg transition duration-150 ease-in-out ml-1"
                        >
                          Understood
                        </button>
                      </div>
                    </div>
                  </div>
                </div>
                {/* start trailer */}
              </div>
            </div>
          </div>
        </div>
      );
    });
  };
  return (
    <div className="py-5 grid phone:grid-cols-1 ipad:grid-cols-3 desktop:grid-cols-4 pc:grid-cols-5 gap-5 px-5">
      {renderContent()}
    </div>
  );
}
