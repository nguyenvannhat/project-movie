import moment from "moment";
import React, { useEffect, useState } from "react";
import { NavLink, useParams } from "react-router-dom";
import { InforFilm } from "../../services/InforFilm";
import { MovieService } from "../../services/MovieService";
import { AiOutlinePlayCircle } from "react-icons/ai";

// trang chi tiết
export default function DetailPage() {
  // lấy id
  let { id } = useParams();

  const [movie, setmovie] = useState({});
  useEffect(() => {
    MovieService.getDetaiMovie(id)
      .then((res) => {
        setmovie(res.data.content);
      })
      .catch((err) => {});
  }, []);

  const [lichChieu, setlichChieu] = useState([]);
  useEffect(() => {
    InforFilm.getThongTinFilm(id)
      .then((res) => {
        setlichChieu(res.data.content.heThongRapChieu);
      })
      .catch((err) => {});
  }, []);

  return (
    <div className="mt-4">
      <div className="w-full h-auto phone:px-2 ipad:px-3 desktop:px-5 pc:px-6 phone:block ipad:flex desktop:flex  pc:flex ">
        <div className="phone:block phone:w-full ipad:block ipad:w-full desktop:flex desktop:w-1/2 pc:flex pc:w-1/2">
          <div className="w-1/2 ">
            <div class="">
              <div class="overflow-hidden bg-no-repeat bg-cover  absolute">
                <img
                  src={movie.hinhAnh}
                  class=" phone:w-48 phone:h-72 ipad:w-64 ipad:h-80 desktop:w-80 desktop:h-96 pc:w-80 pc:h-96 rounded-sm"
                />
                <div class="absolute top-0 right-0 bottom-0 left-0 w-full h-full overflow-hidden bg-fixed opacity-0 hover:opacity-40 transition duration-300 ease-in-out bg-black"></div>
              </div>

              <b
                type="button"
                className="phone:pt-24 ipad:pt-32 desktop:pt-40 pc:pt-44 relative  "
                data-bs-toggle="modal"
                data-bs-target="#exampleModal"
              >
                <AiOutlinePlayCircle className=" w-12 h-14 text-white  border-spacing-0" />
              </b>

              <div
                class="modal fade fixed top-0 left-0 hidden w-full h-full outline-none overflow-x-hidden overflow-y-auto"
                id="exampleModal"
                tabindex="-1"
                aria-labelledby="exampleModalLabel"
                aria-hidden="true"
              >
                <div class="modal-dialog relative w-auto pointer-events-none">
                  <div class="modal-content border-none shadow-lg relative flex flex-col w-full pointer-events-auto bg-white bg-clip-padding rounded-md outline-none text-current">
                    <div class="modal-header flex flex-shrink-0 items-center justify-between p-4 border-b border-gray-200 rounded-t-md">
                      <h5
                        class="text-xl font-medium leading-normal text-gray-800"
                        id="exampleModalLabel"
                      >
                        Trailer
                      </h5>
                      <button
                        type="button"
                        class="btn-close box-content w-4 h-4 p-1 text-black border-none rounded-none opacity-50 focus:shadow-none focus:outline-none focus:opacity-100 hover:text-black hover:opacity-75 hover:no-underline"
                        data-bs-dismiss="modal"
                        aria-label="Close"
                      ></button>
                    </div>
                    <div class="modal-body relative ">
                      <iframe
                        width="460"
                        height="300"
                        src={movie.trailer}
                        title="YouTube video player"
                        frameborder="0"
                        allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture"
                        allowfullscreen
                      ></iframe>
                    </div>
                    <div class="modal-footer flex flex-shrink-0 flex-wrap items-center justify-end p-4 border-t border-gray-200 rounded-b-md">
                      <button
                        type="button"
                        class="px-6
          py-2.5
          bg-purple-600
          text-white
          font-medium
          text-xs
          leading-tight
          uppercase
          rounded
          shadow-md
          hover:bg-purple-700 hover:shadow-lg
          focus:bg-purple-700 focus:shadow-lg focus:outline-none focus:ring-0
          active:bg-purple-800 active:shadow-lg
          transition
          duration-150
          ease-in-out"
                        data-bs-dismiss="modal"
                      >
                        Close
                      </button>
                      <button
                        type="button"
                        class="px-6
      py-2.5
      bg-blue-600
      text-white
      font-medium
      text-xs
      leading-tight
      uppercase
      rounded
      shadow-md
      hover:bg-blue-700 hover:shadow-lg
      focus:bg-blue-700 focus:shadow-lg focus:outline-none focus:ring-0
      active:bg-blue-800 active:shadow-lg
      transition
      duration-150
      ease-in-out
      ml-1"
                      >
                        Save changes
                      </button>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
          <div className=" phone:w-full phone:mt-36  ipad:block  desktop:w-1/2 pc:w-1/2 phone:ml-0 ipad:ml-0 desktop:ml-5 pc:ml-5 text-left desktop:mt-0 pc:mt-0">
            <h5 class="text-gray-900 text-xl font-medium mb-4">
              Tên Phim: {movie.tenPhim}
            </h5>
            <p class="text-gray-700 text-base mb-3">
              {" "}
              <b className="text-base text-red-500 font-medium">Mô tả</b> :{" "}
              {movie.moTa}
            </p>
            <p class="text-gray-600 text-base mb-3">
              <b className="text-base text-red-500 font-medium">
                {" "}
                Ngày khởi chiếu :{" "}
              </b>
              {moment(movie.ngayKhoiChieu).format("DD/MM/YYYY")}
            </p>
            <p class="text-gray-600 text-base mb-3 flex">
              <b className="text-base text-red-500 font-medium">Đánh giá :</b>{" "}
              {(movie.danhGia * 5) / 10}{" "}
              <svg
                aria-hidden="true"
                class="w-7 h-7 text-yellow-400"
                fill="currentColor"
                viewBox="0 0 20 20"
                xmlns="http://www.w3.org/2000/svg"
              >
                <title>First star</title>
                <path d="M9.049 2.927c.3-.921 1.603-.921 1.902 0l1.07 3.292a1 1 0 00.95.69h3.462c.969 0 1.371 1.24.588 1.81l-2.8 2.034a1 1 0 00-.364 1.118l1.07 3.292c.3.921-.755 1.688-1.54 1.118l-2.8-2.034a1 1 0 00-1.175 0l-2.8 2.034c-.784.57-1.838-.197-1.539-1.118l1.07-3.292a1 1 0 00-.364-1.118L2.98 8.72c-.783-.57-.38-1.81.588-1.81h3.461a1 1 0 00.951-.69l1.07-3.292z"></path>
              </svg>{" "}
              / 5{" "}
              <svg
                aria-hidden="true"
                class="w-7 h-7 text-yellow-400"
                fill="currentColor"
                viewBox="0 0 20 20"
                xmlns="http://www.w3.org/2000/svg"
              >
                <title>First star</title>
                <path d="M9.049 2.927c.3-.921 1.603-.921 1.902 0l1.07 3.292a1 1 0 00.95.69h3.462c.969 0 1.371 1.24.588 1.81l-2.8 2.034a1 1 0 00-.364 1.118l1.07 3.292c.3.921-.755 1.688-1.54 1.118l-2.8-2.034a1 1 0 00-1.175 0l-2.8 2.034c-.784.57-1.838-.197-1.539-1.118l1.07-3.292a1 1 0 00-.364-1.118L2.98 8.72c-.783-.57-.38-1.81.588-1.81h3.461a1 1 0 00.951-.69l1.07-3.292z"></path>
              </svg>
            </p>
          </div>
        </div>

        <div className=" phone:w-full ipad:w-1/2 desktop:w-1/2 pc:w-1/2 phone:m-2 ipad:ml-8  desktop:ml-10 pc:ml-10 p-2  shadow-xl text-left">
          <div>
            {lichChieu.map((item) => {
              return (
                <div className="w-full  mb-20 ">
                  <hr />
                  <div className="mb-3">
                    {" "}
                    <b className="text-base text-red-500 font-medium">
                      Tên Rạp:
                    </b>
                    {item.maHeThongRap}
                  </div>
                  <div>
                    {item.cumRapChieu.map((item2) => {
                      return (
                        <div className="mb-3">
                          <b className="text-base text-red-500 font-medium">
                            Địa chỉ :
                          </b>{" "}
                          {item2.diaChi}
                          {item2.lichChieuPhim
                            ?.slice(0, 1)
                            .map((item3, index) => {
                              return (
                                <NavLink
                                  key={index}
                                  to={`/datVe/${item3.maLichChieu}`}
                                >
                                  <div className="mt-3">
                                    <button
                                      type="button"
                                      class="inline-block px-6 py-2.5 bg-green-500 text-white font-medium text-xs leading-tight uppercase rounded shadow-md hover:bg-green-600 hover:shadow-lg focus:bg-green-600 focus:shadow-lg focus:outline-none focus:ring-0 active:bg-green-700 active:shadow-lg transition duration-150 ease-in-out"
                                    >
                                      Đặt vé
                                    </button>
                                  </div>
                                </NavLink>
                              );
                            })}
                        </div>
                      );
                    })}
                  </div>
                </div>
              );
            })}
          </div>
        </div>
      </div>
      <div className="text-left font-normal px-5 pt-5">
        Lorem ipsum dolor sit amet, consectetur adipisicing elit. Asperiores
        quasi ut eius porro excepturi, assumenda fugit. Accusantium laudantium
        deleniti ipsam optio adipisci neque amet nisi omnis maiores excepturi
        nostrum a iusto odit aut aliquam, sapiente ipsum exercitationem sunt sed
        labore fuga facilis. Omnis beatae repellat veritatis similique, tenetur
        eos deserunt laboriosam obcaecati explicabo nisi molestiae
        exercitationem placeat ea reiciendis a eveniet ratione! Molestias
        consequuntur eius laudantium pariatur. Assumenda vitae qui officia
        provident id nam, et laudantium modi maxime atque iste distinctio
        consequatur quos incidunt quisquam sequi praesentium facilis nesciunt
        nostrum beatae sit voluptatem. Eligendi, quia nisi? Veniam iste illo
        quos autem, quod sed, ex veritatis iure exercitationem reprehenderit
        ducimus amet nemo laboriosam, vero officiis? Neque dignissimos amet
        dolores cumque dolorum ab recusandae accusamus laudantium in, odit
        soluta consequatur itaque dolor provident numquam nemo atque optio
        accusantium temporibus. Quia facilis voluptatibus, quidem, ipsum
        reiciendis odit itaque quasi vitae temporibus odio expedita ex ipsam
        esse ab, pariatur in natus maiores veniam eaque rerum est ullam dolorem
        dignissimos? Minus, voluptatem. Nemo rerum deleniti voluptatem beatae
        aut non sint fugit sed temporibus tempora, eligendi corporis fuga
        asperiores ea aspernatur. Delectus placeat fugit necessitatibus impedit,
        voluptatum voluptatibus quibusdam, deleniti ut reprehenderit rem qui
        velit ipsa?
      </div>
    </div>
  );
}
