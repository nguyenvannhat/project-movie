import {
  Button,
  Cascader,
  DatePicker,
  Form,
  Input,
  InputNumber, 
  Radio,
  Select,
  Switch,
  TreeSelect,
} from "antd";
import React, { useEffect, useState } from "react";

import { useFormik } from "formik";
import moment from "moment";
// import { useDispatch } from "react-redux";
import { ThemPhimUploadHinhAction } from "../../../redux/action/QuanLyPhimAction";
import { useDispatch } from "react-redux";
import { MovieService } from "../../../services/MovieService";
// import { MovieService } from "../../../services/MovieService";

const AddFilm = () => {
  const [Img, setImg] = useState("");
  // let dispatch = useDispatch();
  let dispatch = useDispatch();

  let manhom = "GP12";

  const formik = useFormik({
    initialValues: {
      tenPhim: "",
      trailer: "",
      moTa: "",
      ngayKhoiChieu: "",
      dangChieu: false,
      sapChieu: false,
      hot: false,
      danhGia: 0,
      hinhAnh: {},
    },
    onSubmit: (values) => {
      console.log("values", values);
      values.maNhom = "maNhom=GP12";
      // tao o day
      let formData = new FormData();
      formData.append("tenPhim", formik.values.tenPhim);

      // console.log("fomik ", formData.get("tenPhim"));
      for (let key in values) {
        formData.append(key, values[key]);
        if (key !== "hinhAnh") {
          formData.append(key, values[key]);
        } else {
          formData.append("file", values.hinhAnh, values.hinhAnh.name);
        }
      }
      // dispatch(ThemPhimUploadHinhAction(formData));
      MovieService.postThemPhimUpLoadHinh(formData)
        .then((res) => {
          console.log("res", res.data.content);
          // dispatch(ThemPhimUploadHinhAction(formData));
        })
        .catch((err) => {
          console.log(err);
        });
    },
  });

  //
  const handelChangeSwitch = (name) => {
    return (value) => {
      formik.setFieldValue(name, value);
    };
  };

  const handleChangeInputNumber = (name) => {
    return (value) => {
      formik.setFieldValue(name, value);
    };
  };
  const handleChangeFile = (e) => {
    let file = e.target.files[0];
    if (file.type === "image/jpg" || file.type === "image/png") {
      let render = new FileReader();
      render.readAsDataURL(file);
      render.onload = (e) => {
        console.log(e.target.result);
        setImg(e.target.result);
      };
      // đem dữ liệu vào formik
      console.log(file, "file");
      formik.setFieldValue("hinhAnh", file);
    }
  };

  let handleChangeDatePicker = (value) => {
    // console.log("handleChangeDatePicker", moment(params.format("DD/MM/YYYY")));
    let ngayKhoiChieu = moment(value).format("DD/MM/YYYY");
    formik.setFieldValue("ngayKhoiChieu", ngayKhoiChieu);
  };

  const [componentSize, setComponentSize] = useState("default");

  const onFormLayoutChange = ({ size }) => {
    setComponentSize(size);
  };

  return (
    <Form
      onSubmitCapture={formik.handleSubmit}
      labelCol={{
        span: 4,
      }}
      wrapperCol={{
        span: 14,
      }}
      layout="horizontal"
      initialValues={{
        size: componentSize,
      }}
      onValuesChange={onFormLayoutChange}
      size={componentSize}
    >
      <Form.Item label="Form Size" name="size">
        <Radio.Group>
          <Radio.Button value="small">


            anie's shop is among the library and a diner
            
            
            </Radio.Button>
          <Radio.Button value="default">Default</Radio.Button>
          <Radio.Button value="large">Large</Radio.Button>
        </Radio.Group>
      </Form.Item>
      <Form.Item label="Ten phim">
        <Input name="tenPhim" onChange={formik.handleChange} />
      </Form.Item>
      <Form.Item label="trailer">
        <Input name="trailer" onChange={formik.handleChange} />
      </Form.Item>
      <Form.Item label="Mô Tả">
        <Input name="moTa" onChange={formik.handleChange} />
      </Form.Item>
      <Form.Item label="Ngày khởi chiếu">
        <DatePicker format={"DD//MM/YYYY"} onChange={handleChangeDatePicker} />
      </Form.Item>
      <Form.Item label="Đang chiếu ">
        <Switch name="dangChieu" onChange={handelChangeSwitch("dangChieu")} />
      </Form.Item>
      <Form.Item label="Sắp chiếu">
        <Switch name="sapChieu" onChange={handelChangeSwitch("sapChieu")} />
      </Form.Item>
      <Form.Item label="Hot">
        <Switch name="Hot" onChange={handelChangeSwitch("sapChieu")} />
      </Form.Item>

      {/* <Form.Item label="Select">
        <Select>
          <Select.Option value="demo">Demo</Select.Option>
        </Select>
      </Form.Item> */}

      <Form.Item label="số sao">
        <InputNumber
          name="Hot"
          onChange={handleChangeInputNumber("danhGia")}
          min={1}
          max={10}
        />
      </Form.Item>
      <Form.Item label="hình ảnh">
        <input
          type="file"
          onChange={handleChangeFile}
          accept="image/png , image/jpg"
        />
        <br />
        <img className="w-36 h-36" src={Img} alt="..." />
      </Form.Item>

      <Form.Item label="Button">
        <button type="submit">them phim</button>
      </Form.Item>
    </Form>
  );
};

export default AddFilm;
