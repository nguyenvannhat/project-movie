import { Button, Table } from "antd";
import React, { Fragment, useEffect } from "react";
import { AudioOutlined } from "@ant-design/icons";
import { Input, Space } from "antd";
import Search from "antd/lib/input/Search";
import { useDispatch, useSelector } from "react-redux";
import QuanLyPhimAction from "../../../redux/action/QuanLyPhimAction";
import { NavLink, useHistory } from "react-router-dom";

export default function Film() {
  let history = useHistory();
  let layDanhSachPhim = useSelector(
    (state) => state.QuanLyPhimReducer.ArrayFilm
  );
  let dispatch = useDispatch();

  useEffect(() => {
    dispatch(QuanLyPhimAction());
  }, []);
  // console.log("layDanhSachPhim", layDanhSachPhim);
  const columns = [
    {
      title: "maPhim",
      dataIndex: "maPhim",

      sorter: (a, b) => a.maPhim - b.maPhim,
      sortDirections: ["descend"],
    },
    {
      title: "hinhAnh",
      dataIndex: "hinhAnh",
      render: (text, film) => {
        return (
          <Fragment>
            <img src={film.hinhAnh} alt={film.tenPhim} className="w-12 h-10" />
          </Fragment>
        );
      },
      defaultSortOrder: "descend",
      sorter: (a, b) => a.age - b.age,
    },
    {
      title: "tên phim",
      dataIndex: "tenPhim",
      filters: [
        {
          text: "London",
          value: "London",
        },
        {
          text: "New York",
          value: "New York",
        },
      ],
      onFilter: (value, record) => record.address.indexOf(value) === 0,
    },
    {
      title: "hành động",
      dataIndex: "hanhdong",
      render: (text, film) => {
        return (
          <Fragment>
            <NavLink to="/edit">
              <button className="bg-cyan-500 w-12 h-10 rounded-sm mr-2">
                sửa
              </button>
            </NavLink>
            <NavLink to="/delete">
              <button className="bg-amber-600 w-12 h-10 rounded-sm ml-1">
                xóa
              </button>{" "}
            </NavLink>
          </Fragment>
        );
      },
      defaultSortOrder: "descend",
      sorter: (a, b) => a.age - b.age,
    },
  ];
  const data = layDanhSachPhim;

  const suffix = (
    <AudioOutlined
      style={{
        fontSize: 16,
        color: "#1890ff",
      }}
    />
  );

  const onSearch = (value) => console.log(value);
  const onChange = (pagination, filters, sorter, extra) => {
    // console.log("params", pagination, filters, sorter, extra);
  };

  return (
    <div>
      <Button
        onClick={() => {
          history.push("/admin/addFilm");
        }}
      >
        thêm phim
      </Button>
      <Search
        placeholder="input search text"
        allowClear
        enterButton="Search"
        size="large"
        onSearch={onSearch}
        className="mb-2"
      />

      <Table columns={columns} dataSource={data} onChange={onChange} />
    </div>
  );
}
