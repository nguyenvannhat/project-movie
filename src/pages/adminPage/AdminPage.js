import {
  MenuFoldOutlined,
  MenuUnfoldOutlined,
  UserOutlined,
  FileOutlined,
} from "@ant-design/icons";
import { Layout, Menu } from "antd";
import SubMenu from "antd/lib/menu/SubMenu";
import React, { useState } from "react";
import { NavLink, Redirect, Route, Router } from "react-router-dom";
import AddFilm from "./AddFilm/AddFilm";

import Film from "./Film/Film";
import Showtime from "./showtime/Showtime";
import User from "./user/User";
const { Header, Sider, Content } = Layout;

const AdminPage = () => {
  let USER_LOGIN = "user";
  const [collapsed, setCollapsed] = useState(false);
  if (!localStorage.getItem(USER_LOGIN)) {
    alert("ban chua dang nhap vui long dang nhap");
    return <Redirect to="/login" />;
  }
  // if (!localStorage.maLoaiNguoiDung !== "QuanTri") {
  //   alert("ban khong co quyen vao trang nay ");
  //   return <Redirect to="/" />;
  // }
  return (
    <Layout className="py-2 h-auto">
      <Sider trigger={null} collapsible collapsed={collapsed}>
        <div className="bg-slate-300 h-16">
          <img
            src="https://www.galaxycine.vn/website/images/galaxy-logo.png"
            alt=""
            className="pt-3"
          />
        </div>
        <Menu theme="dark" mode="inline" defaultSelectedKeys={["1"]}>
          <Menu.Item key="1" icon={<UserOutlined />}>
            <NavLink to="/admin/users">Users</NavLink>
          </Menu.Item>

          <SubMenu
            key="sub1"
            theme="dark"
            mode="inline"
            icon={<FileOutlined />}
            title="   Films"
          >
            {" "}
            <Menu.Item key="2" icon={<UserOutlined />}>
              <NavLink to="/admin/film">Film</NavLink>
            </Menu.Item>
            <Menu.Item key="3" icon={<UserOutlined />}>
              <NavLink to="/admin/addFilm">add new</NavLink>
            </Menu.Item>
          </SubMenu>

          <Menu.Item key="4" icon={<UserOutlined />}>
            <NavLink to="/admin/showtime">showtime</NavLink>
          </Menu.Item>
        </Menu>
      </Sider>
      <Layout className="site-layout">
        <Header
          className="site-layout-background"
          style={{
            padding: 0,
          }}
        >
          {React.createElement(
            collapsed ? MenuUnfoldOutlined : MenuFoldOutlined,
            {
              className: "trigger",
              onClick: () => setCollapsed(!collapsed),
            }
          )}
        </Header>
        <Content
          className="site-layout-background"
          style={{
            margin: "24px 16px",
            padding: 24,
            minHeight: 280,
          }}
        >
          <Route path="/admin/users">
            <User />
          </Route>
          <Route path="/admin/film">
            <Film />
          </Route>
          <Route path="/admin/addFilm">
            <AddFilm />
          </Route>
          <Route path="/admin/showtime">
            <Showtime />
          </Route>
        </Content>
      </Layout>
    </Layout>
  );
};

export default AdminPage;
