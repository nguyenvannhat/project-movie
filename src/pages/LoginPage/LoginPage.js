import React from "react";
import FormLogin from "./FormLogin/FormLogin";
// LoginPage trang đăng nhập
import Lottie from "lottie-react";
import loginAnimate from "../../assets/login-animate.json";

const LoginPage = () => {
  return (
    <div className="">
      <div className="w-full mb-4">
        <img
          src="https://img4.thuthuatphanmem.vn/uploads/2020/12/25/background-ban-go-lam-viec_101227484.jpg"
          alt=""
          className="w-full h-600 absolute bg-repeat"
        />

        <div className="flex w-full ">
          <div className="w-1/2 relative  phone:hidden ipad:block desktop:block pc:block">
            <Lottie
              animationData={loginAnimate}
              className="h-auto w-620  relative"
            />
          </div>
          <div className="w-1/2   relative  ">
            {" "}
            <FormLogin />
          </div>
        </div>
      </div>
    </div>
  );
};

export default LoginPage;
