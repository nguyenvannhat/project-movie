import React from "react";
import { Button, Checkbox, Form, Input, message } from "antd";
import { userService } from "../../../services/userService";
import { logcalStorageService } from "../../../services/localStorageService";
import { Link, NavLink, useHistory } from "react-router-dom";
import { useDispatch } from "react-redux";
import { setUserInforAtion } from "../../../redux/action/userAction";

export default function FormLogin() {
  // đăng nhập thành công chuyển hướng sang trang chủ
  let history = useHistory();
  let dispatch = useDispatch();
  const onFinish = (values) => {
    // console.log("Success:", values);
    userService
      .postDangNhap(values)
      .then((res) => {
        // console.log(res);
        logcalStorageService.setUserInfor(res.data.content);
        // dispatch redux
        dispatch(setUserInforAtion(res.data.content));
        // thông báo đăng nhập thành công from antd
        message.success("Đăng nhập thành công ");
        // setTimeout để làm chậm quá trình chuyển hướng
        setTimeout(() => {
          // gọi history để chuyển hướng nến đăng nhập thành công
          history.push("/");
        }, 3000);
      })
      .catch((err) => {
        // thông báo đăng nhập thất bại from antd , err.response.data.content từ local
        message.error(err.response.data.content);
      });
  };

  const onFinishFailed = (errorInfo) => {
    // console.log("Failed:", errorInfo);
  };

  return (
    <Form
      className="w-96 h-96 rounded shadow-2xl  text-white mt-32  bg-white opacity-50 border-spacing-2 "
      name="basic"
      layout="vertical"
      initialValues={{
        remember: true,
      }}
      onFinish={onFinish}
      onFinishFailed={onFinishFailed}
      autoComplete="off"
    >
      <div className="pt-2">
        <Form.Item
          label="Username"
          name="taiKhoan" // sửa lại taiKhoan để backend bốc ra (open network để xem)
          rules={[
            {
              required: true,
              message: "Please input your username!",
            },
          ]}
        >
          <Input />
        </Form.Item>

        <Form.Item
          label="Password"
          name="matKhau" // sửa lại matKhau để backend bốc ra (open network để xem)
          rules={[
            {
              required: true,
              message: "Please input your password!",
            },
          ]}
        >
          <Input.Password />
        </Form.Item>

        <Form.Item name="remember" valuePropName="checked">
          <Checkbox>Remember me</Checkbox>
        </Form.Item>

        <div className="text-center">
          <Button className="bg-blue-500 text-white" htmlType="submit">
            Đăng Nhập
          </Button>

          <NavLink to={"/dangKy"}>
            {" "}
            <p className="text-black pt-4 pb-2">Bạn chưa có tài khoản</p>
            <Button className="bg-blue-500 text-white" htmlType="submit">
              Đăng ký
            </Button>
          </NavLink>
        </div>
      </div>
    </Form>
  );
}
