import "./App.css";
import { BrowserRouter, Route, Router, Switch } from "react-router-dom";
import { userRoutes } from "./routes/userRoutes";

function App() {
  return (
    <div className="App">
      <BrowserRouter>
        <Switch>
          {userRoutes.map((route) => {
            if (route.isUseLayout) {
              return (
                <Route
                  exact={route.exact}
                  path={route.path}
                  render={() => {
                    return route.component;
                  }}
                />
              );
            }
            return (
              <Route
                exact={route.exact}
                path={route.path}
                component={route.component}
              />
            );
          })}
          ;
        </Switch>
      </BrowserRouter>
    </div>
  );
}

export default App;
