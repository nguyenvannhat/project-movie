import DetailPage from "../pages/DetailPage/DetailPage";
import HomePage from "../pages/HomePage/HomePage";
import LoginPage from "../pages/LoginPage/LoginPage";

import { LayoutThem } from "../HOC/LayoutThem";

import Datve from "../pages/DatVe/Datve";
import Support from "../pages/support/Support";
import MovieTabs from "../pages/HomePage/MovieTabs/MovieTabs";
import ChiTiet from "../pages/HomePage/promotion/ChiTiet";

import GiaVe from "../pages/GiaVe/GiaVe";
import PageFilm from "../pages/ListPhim/PageFilm";
import trangDangKy from "../pages/DangKy/trangDangKy";

import AdminPage from "../pages/adminPage/AdminPage";

// quản lý các trang
export const userRoutes = [
  {
    path: "/",
    component: <LayoutThem Component={HomePage} />,
    exact: true,
    isUseLayout: true,
  },
  {
    path: "/detail/:id",
    component: <LayoutThem Component={DetailPage} />,
    isUseLayout: true,
  },
  {
    path: "/muave",
    component: <LayoutThem Component={MovieTabs} />,
    isUseLayout: true,
  },
  {
    path: "/khuyenMai",
    component: <LayoutThem Component={ChiTiet} />,
    isUseLayout: true,
  },
  {
    path: "/PageFilm",
    component: <LayoutThem Component={PageFilm} />,
    isUseLayout: true,
  },

  {
    path: "/login",
    component: <LayoutThem Component={LoginPage} />,
    isUseLayout: true,
  },
  {
    path: "/datVe/:id",
    component: <LayoutThem Component={Datve} />,
    isUseLayout: true,
  },
  {
    path: "/support",
    component: <LayoutThem Component={Support} />,
    isUseLayout: true,
  },
  {
    path: "/giaVe",
    component: <LayoutThem Component={GiaVe} />,
    isUseLayout: true,
  },

  {
    path: "/dangKy",
    component: <LayoutThem Component={trangDangKy} />,
    isUseLayout: true,
  },
  {
    path: "/admin",
    component: AdminPage,
  },
];
