import React from "react";
import Footer from "../components/Footer/Footer";
import HeaderTheme from "../components/HeaderLayot/HeaderTheme";

export function LayoutThem({ Component }) {
  return (
    <div>
      <HeaderTheme />
      <Component />
      <Footer />
    </div>
  );
}
